(function ($, Drupal, drupalSettings) {

  'use strict';

  /**
   * Init pace API for the textfield to be able to use autocomplete.
   */
  Drupal.behaviors.placeAutocomplete = {
    initPlaceAutocomplete: function (input) {

      const locationInitialize = function () {
        const autocomplete = new google.maps.places.Autocomplete(input);

        // Set up filter by country.
        const countryCode = drupalSettings.place.autocomplete;
        if (countryCode.length > 0) {
          autocomplete.setComponentRestrictions({ country: countryCode });
        }

        // Trigger search on select.
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
          $(input).closest('form')
            .find('.form-submit')
            .trigger('click');
        });
      }

      google.maps.event.addDomListener(window, 'load', locationInitialize);
    },
    attach: function (context) {
      const self = this;

      if (typeof drupalSettings.place === 'undefined') {
        console.error('Place settings not found');
        return;
      }

      $(context).find('.location-input').once('locationInput').each(function () {
        self.initPlaceAutocomplete(this);
      });
    }
  }
})(jQuery, Drupal, drupalSettings);
