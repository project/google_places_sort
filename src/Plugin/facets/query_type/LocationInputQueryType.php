<?php

namespace Drupal\google_places_sort\Plugin\facets\query_type;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\facets\QueryType\QueryTypePluginBase;
use Drupal\facets\Result\Result;
use Drupal\geocoder\GeocoderInterface;
use Drupal\search_api\Query\QueryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Support for location input facets within the Search API scope.
 *
 * @FacetsQueryType(
 *   id = "location_input_query_type",
 *   label = @Translation("Location Input"),
 * )
 */
class LocationInputQueryType extends QueryTypePluginBase implements ContainerFactoryPluginInterface {

  /**
   * The geocoder service.
   *
   * @var \Drupal\geocoder\GeocoderInterface
   */
  protected $geocoder;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, GeocoderInterface $geocoder, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->geocoder = $geocoder;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('geocoder'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function execute() {
    $active_items = $this->facet->getActiveItems();

    // Nothing to do if there is no active items for the facet.
    if (!$active_items || !is_array($active_items) || empty($active_items[0])) {

      // Let's provide a default sort by created timestamp.
      $sorts = &$this->query->getSorts();
      $sorts = [
        'title' => QueryInterface::SORT_ASC,
      ];
      return;
    }

    // Get location input value from the facet.
    $location_input = reset($active_items);

    // Presume Belgium for postal codes.
    $matches = [];
    preg_match('/^\d+$/', $location_input, $matches);
    if (count($matches)) {
      $location_input .= ' Belgium';
    }

    // Load geocode providers.
    $widget_config = $this->facet->getWidgetInstance()->getConfiguration();
    $providers = $this->entityTypeManager->getStorage('geocoder_provider')
      ->loadMultiple($widget_config['providers']);

    // Geocode location input to get lat and lon.
    $geocoded_addresses = $this->geocoder->geocode($location_input, $providers);

    // Log error message and abort the query.
    if (!$geocoded_addresses) {
      $this->query->abort($this->t("The location input is not valid and can't be geocoded"));
      return;
    }

    $geocoded_addresses = $geocoded_addresses->first()->getCoordinates();

    $location_options[] = [
      'field' => $this->facet->getFieldIdentifier(),
      'lat' => $geocoded_addresses->getLatitude(),
      'lon' => $geocoded_addresses->getLongitude(),
      // We have no filter by radius, so we use change default operator from <
      // to >= to have no limits by radius.
      'filter_query_conditions' => [
        'operator' => '>=',
      ],
    ];

    $this->query->setOption('search_api_location', $location_options);
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Only one facet result matters for the textfield.
    $this->facet->setResults([
      new Result($this->facet, 'location-input', $this->facet->label(), 1),
    ]);

    return $this->facet;
  }

}
