<?php

namespace Drupal\google_places_sort\Plugin\facets\widget;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\facets\FacetInterface;
use Drupal\facets\Widget\WidgetPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The location input widget.
 *
 * @FacetsWidget(
 *   id = "location_input",
 *   label = @Translation("Location Input"),
 *   description = @Translation("A widget that shows a location input field"),
 * )
 */
class LocationInputWidget extends WidgetPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface $form_builder, EntityFieldManagerInterface $entity_field_manager, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->formBuilder = $form_builder;
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $configuration = parent::defaultConfiguration();
    $configuration['field'] = NULL;
    $configuration['placeholder'] = NULL;
    $configuration['button_label'] = NULL;
    $configuration['providers'] = [];

    return $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, FacetInterface $facet) {
    $config = $this->getConfiguration();

    $form = parent::buildConfigurationForm($form, $form_state, $facet);

    $form['placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Field placeholder'),
      '#description' => $this->t('Enter the placeholder for the location input field.'),
      '#default_value' => $config['placeholder'] ?? NULL,
    ];

    $form['button_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button label'),
      '#description' => $this->t('Enter the title of the submit button.'),
      '#default_value' => $config['button_label'] ?? NULL,
      '#required' => TRUE,
    ];

    // Get all fields with type 'geofield'.
    $fields = $this->entityFieldManager->getFieldMapByFieldType('geofield');

    // Make sure there are fields added to node.
    if (!isset($fields['node'])) {
      $this->messenger()->addWarning($this->t('There is no available fields with type "geofield".'));
      return $form;
    }

    $options = [];
    $storage = $this->entityTypeManager->getStorage('node_type');

    foreach ($fields['node'] as $field_name => $info) {
      $bundles = $info['bundles'];

      foreach ($bundles as $bundle) {
        $node_type = $storage->load($bundle);
        $options[$node_type->label()]["{$bundle}:{$field_name}"] = $field_name;
      }
    }

    $form['field'] = [
      '#type' => 'select',
      '#title' => $this->t('Get geocoder settings from'),
      '#description' => $this->t('Geocoder settings will be used from the chosen field.'),
      '#options' => $options,
      '#required' => TRUE,
      '#default_value' => $config['field'] ?? NULL,
    ];

    /** @var \Drupal\geocoder\ProviderPluginManager $provider_manager */
    $provider_manager = \Drupal::service('plugin.manager.geocoder.provider');
    $providers = $provider_manager->getPlugins();

    $form['providers'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Activated providers'),
      '#description' => $this->t('The providers will be set automatically after saving of the facet.'),
      '#options' => array_column($providers, 'name', 'id'),
      '#default_value' => $config['providers'] ?? [],
      '#element_validate' => [[self::class, 'saveProviders']],
      '#disabled' => TRUE,
    ];

    return $form;
  }

  /**
   * Extract and save providers from the chosen field.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function saveProviders($element, FormStateInterface $form_state) {
    // Extract bundle and field name from the field setting of the facet.
    [$bundle, $field_name] = explode(':', $form_state->getValue(['widget_config', 'field']));

    // Load field config to get providers.
    $storage = \Drupal::entityTypeManager()->getStorage('field_config');
    $field_config = $storage->load("node.{$bundle}.{$field_name}");

    // Set providers from the field to the element.
    $providers = $field_config->getThirdPartySetting('geocoder_field', 'providers');
    $form_state->setValueForElement($element, $providers);
  }

  /**
   * {@inheritdoc}
   */
  public function build(FacetInterface $facet) {
    return $this->formBuilder->getForm('\Drupal\google_places_sort\Form\LocationInputForm', $facet);
  }

}
