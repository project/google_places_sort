<?php

namespace Drupal\google_places_sort\Plugin\facets\processor;

use Drupal\facets\FacetInterface;
use Drupal\facets\Processor\BuildProcessorInterface;
use Drupal\facets\Processor\ProcessorPluginBase;

/**
 * Processor implementation for location input.
 *
 * @FacetsProcessor(
 *   id = "location_input_processor",
 *   label = @Translation("Location input processor"),
 *   description = @Translation("Processor that handles location input queries."),
 *   stages = {
 *     "build" = 35
 *   }
 * )
 */
class LocationInputProcessor extends ProcessorPluginBase implements BuildProcessorInterface {

  /**
   * {@inheritdoc}
   */
  public function getQueryType() {
    return 'location_input';
  }

  /**
   * {@inheritdoc}
   */
  public function build(FacetInterface $facet, array $results) {
    return $results;
  }

}
