<?php

namespace Drupal\google_places_sort\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\facets\FacetInterface;

/**
 * Provides the Form to be used in the Location Input Facet.
 */
class LocationInputForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'location_input_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, FacetInterface $facet = NULL) {
    $active_items = $facet->getActiveItems();
    $form['#attributes']['class'][] = 'form--inline';
    $form['#attributes']['class'][] = 'clearfix';
    $form['location'] = [
      '#type' => 'textfield',
      '#title' => $facet->label(),
      '#attributes' => [
        'class' => ['location-input'],
      ],
      '#default_value' => $active_items[0] ?? NULL,
    ];

    $facet_config = $facet->getWidgetInstance()->getConfiguration();

    if (!empty($facet_config['placeholder'])) {
      $form['location']['#attributes']['placeholder'] = $facet_config['placeholder'];
    }

    $config = $this->config('google_places_sort.autocomplete_settings');

    // Add google map key to the header to get place api working.
    if ($config->get('enabled')) {
      $google_map_key = [
        '#tag' => 'script',
        '#attributes' => ['src' => '//maps.googleapis.com/maps/api/js?key=' . $config->get('place_api_key') . '&libraries=places'],
      ];
      $form['location']['#attached']['html_head'][] = [$google_map_key, 'googleMapKey'];
      $form['location']['#attached']['drupalSettings']['place']['autocomplete'] = $config->get('place_country');
      $form['location']['#attached']['library'][] = 'google_places_sort/location';
    }

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $facet_config['button_label'] ?? $this->t('Search'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\facets\FacetInterface $facet */
    $facet = $form_state->getBuildInfo()['args'][0];
    $url_alias = $facet->getUrlAlias();

    // Get entered value by user.
    $location_raw = $form_state->getValue('location');

    /** @var \Drupal\facets\Plugin\facets\processor\UrlProcessorHandler $url_processor_handler */
    $url_processor_handler = $facet->getProcessors()['url_processor_handler'];
    $url_processor = $url_processor_handler->getProcessor();
    $filter_key = $url_processor->getFilterKey();

    // Create url from current request.
    $request = $this->getRequest();
    $url = Url::createFromRequest($request);
    $query = $request->query->all();

    // Remove the existing value from the url.
    if (isset($query[$filter_key])) {
      foreach ($query[$filter_key] as $id => $filter) {
        if (strpos($filter . $url_processor->getSeparator(), $url_alias) === 0) {
          unset($query[$filter_key][$id]);
        }
      }
    }

    // Clean up empty query.
    if (isset($query[$filter_key]) && empty($query[$filter_key])) {
      unset($query[$filter_key]);
    }

    // Add location value to the url.
    if ($location_raw) {
      $query[$filter_key][] = $url_alias . $url_processor->getSeparator() . $location_raw;
    }

    // Update url query.
    $url->setOption('query', $query);

    // Redirect page with the result url and correct query.
    $form_state->setRedirectUrl($url);
  }

}
