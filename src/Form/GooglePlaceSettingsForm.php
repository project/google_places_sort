<?php

namespace Drupal\google_places_sort\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;

/**
 * Class GooglePlaceSettingsForm.
 *
 * @package Drupal\google_places_sort\Form
 */
class GooglePlaceSettingsForm extends ConfigFormBase {

  /**
   * The country manager.
   *
   * @var \Drupal\Core\Locale\CountryManagerInterface
   */
  protected $countryManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->countryManager = $container->get('country_manager');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'google_places_sort.autocomplete_settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'google_places_sort_autocomplete_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $countries = $this->countryManager->getStandardList();
    foreach ($countries as $key => $country) {
      $countries[$key] = (string) $country;
    }

    $config = $this->config('google_places_sort.autocomplete_settings');

    $form['place_autocomplete'] = [
      '#type' => 'details',
      '#title' => $this->t('Google Place Autocomplete API settings'),
      '#open' => TRUE,
    ];

    $form['place_autocomplete']['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Is enabled?'),
      '#default_value' => $config->get('enabled') ?? FALSE,
      '#description' => $this->t('Whether to use google place autocomplete for the location input.'),
    ];

    $form['place_autocomplete']['country'] = [
      '#type' => 'select',
      '#title' => $this->t('Country'),
      '#options' => $countries,
      '#empty_option' => $this->t('- All -'),
      '#empty_value' => '',
      '#default_value' => $config->get('place_country') ?? '',
      '#description' => $this->t('Restrict the results based on country.'),
    ];

    $google_api_link = Url::fromUri('https://developers.google.com/maps/documentation/javascript/get-api-key', [
      'attributes' => ['target' => '_blank'],
    ]);
    $form['place_autocomplete']['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Google Maps API Key'),
      '#size' => 60,
      '#default_value' => $config->get('place_api_key'),
      '#description' => $this->t('A free API key is needed to use the Google Maps. @click here to generate the API key', [
        '@click' => Link::fromTextAndUrl($this->t('Click here'), $google_api_link)->toString(),
      ]),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory()->getEditable('google_places_sort.autocomplete_settings')
      ->set('place_country', $form_state->getValue('country'))
      ->set('place_api_key', $form_state->getValue('api_key'))
      ->set('enabled', $form_state->getValue('enabled'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
