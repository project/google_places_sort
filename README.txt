INTRODUCTION
------------

This module provides a custom facet "Location input" which allows a user to enter location by using Google Place API (autocomplete) and get results sorted by distance to the entered location.

REQUIREMENTS
-------------
* Search API (https://www.drupal.org/project/search_api)
* Search API Solr (https://www.drupal.org/project/search_api_solr)
* Facets (https://www.drupal.org/project/facets)
* Search API Location (https://www.drupal.org/project/search_api_location)
* Geocoder (https://www.drupal.org/project/geocoder) >=8.x-3.x


TECHNICAL BACKGROUND
-------------
* The "Location input" facet implements a custom query type
  * It checks if there is active results for the facet and gets raw location address
  * The raw location address is being geocoded to get lat and lon by using providers configured in the field
  * All collected information is used by "search_api_location" query option
  * It does not filter results because there is custom "filter_query_conditions" to avoid filtering by radius
    * If you want to filter results by radius you need to remove that condition and pass "radius" value together with lat and lon
  * The sorting will be enabled automatically by "search_api_location" if the views uses sorting by distance. See below how to properly add it
* You can check how search_api_location works with the sorting by looking at SearchApiSolrBackend::setSpatial() in the search_api_solr module

INSTALLATION AND CONFIGURATION
-------------
* Enable this module with all required modules.
* Add a geofield to your search index
  * You should use LatLong Pair subfield
  * Chose Latitude/Longitude as field type
  * Reindex content
* Create a new facet at /admin/config/search/facets
  * Use your geofield for your facet source
  * Use "Location inout" as widget
  * Configure facet by selecting a geofield from the list. Providers will be taken automatically
  * Select "Location input processor"
* Go to the view and add sorting by distance
  * It should be your geofield >> LatLong Pair (distance)
* Enable Google Place API autocomplete at /admin/config/google-place-autocomplete/settings
  * You can use the same API key as Geofield uses for Google Maps provider
* Display view and facet on a page
